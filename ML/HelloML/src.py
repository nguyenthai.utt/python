import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

def generate_noisy_data(m, b, n=100):
    x = tf.random.uniform(shape=(n,))
    noise = tf.random.normal(shape=(len(x),), stddev=0.15)
    y = m*x + b + noise
    return x, y

x_train, y_train = generate_noisy_data(1, 2)

class LRLayer(tf.keras.layers.Layer):
    def __init__(seft):
        super(LRLayer, seft).__init__()
        seft.m = seft.add_weight(shape=(1,), initializer='random_normal')
        seft.b = seft.add_weight(shape=(1,), initializer='zeros')
    
    def call(seft, input):
        return seft.m * input + seft.b

linearLayer = LRLayer()
y_pre = linearLayer(x_train)

def MSE(y_predict, y_true):
    return tf.reduce_mean(tf.square(y_predict - y_true))

learning_rate = 0.05
epochs = 30

mse_loss = []
for i in range(epochs):
    with tf.GradientTape() as tape:
        predictions = linearLayer(x_train)
        cur_mse = MSE(predictions, y_train)
    gradients = tape.gradient(cur_mse, linearLayer.trainable_variables)
    linearLayer.m.assign_sub(learning_rate*gradients[0])
    linearLayer.b.assign_sub(learning_rate*gradients[1])
    mse_loss.append(cur_mse)
x_min = min(min(x_train), -0.15)
x_max = max(max(x_train), 1.15)
x = np.linspace(x_min, x_max)
plt.plot(x, linearLayer.m * x + linearLayer.b, 'r')
plt.plot(x_train, y_train, 'b.')
plt.show()