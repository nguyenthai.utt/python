from Bio import Align
from Bio.Seq import Seq

seqA = Seq("ACAATCC")
seqB = Seq("AGCATGC")
aligner = Align.PairwiseAligner()
aligner.match_score = 1
aligner.mismatch_score = 0
aligner.gap_score = -1
aligner.mode = "local"
score = aligner.score(seqA, seqB)
alignment = aligner.align(seqA, seqB)
print(alignment[0])
print(score)
print(aligner.algorithm)