import matplotlib.pyplot as plt
import numpy as np
import wave
import scipy.signal



sf = wave.open('khosothunhu.wav','r')

signal = sf.readframes(sf.getnframes())
signal = np.fromstring(signal, np.short)
# Tan so lay mau
fs = sf.getframerate()
timeLen = len(signal)/fs
winLen = fs*20/1000
silenceThres = 300
isStart = True
isEnd = False
for idx, sig in enumerate(signal):
    sumSig = 0
    if idx >= len(signal) - winLen : break
    for i in range(idx, idx + int(winLen)):
        sumSig += signal[i]
    sumSig /= winLen
    if (np.abs(sumSig) < silenceThres):
        if(isStart):  
            print("Start: ", idx*timeLen/len(signal))
            isStart = False
            isEnd = True
    else: 
        if(isEnd):
            print("End: ", idx*timeLen/len(signal))
    
# print(len(signal))
time=np.linspace(0, timeLen, len(signal))

# plt.title("Signal")
# plt.plot(time, signal)
# plt.xlabel("time(s)")
# plt.show()
# sf.close


# def ste(x, win):
#   if isinstance(win, str):
#     win = scipy.signal.get_window(win, max(1, len(x) // 8))
#   win = win / len(win)
#   return scipy.signal.convolve(x**2, win**2, mode="same")

# W = ste(signal, scipy.signal.get_window("hamming", int(winLen)))

# plt.title("STE")
# plt.plot(time, W)
# plt.xlabel("time(s)")
# plt.show()
# sf.close

# def _sgn(x):
#   y = np.zeros_like(x)
#   y[np.where(x >= 0)] = 1.0
#   y[np.where(x < 0)] = -1.0
#   return y

# def stzcr(x, win):
#   """Compute short-time zero crossing rate."""
#   if isinstance(win, str):
#     win = scipy.signal.get_window(win, max(1, len(x) // 8))
#   win = 0.5 * win / len(win)
#   x1 = np.roll(x, 1)
#   x1[0] = 0.0
#   abs_diff = np.abs(_sgn(x) - _sgn(x1))
#   return scipy.signal.convolve(abs_diff, win, mode="same")

# rate = stzcr(signal, scipy.signal.get_window("boxcar", 201))

# plt.title("ZCR")
# plt.plot(time, rate)
# plt.xlabel("time(s)")
# plt.show()
# sf.close
























#print(len(signal))

# print("Tan so lay mau : ", fs, "Hz")
# print("So bit bieu dien: ", sf.getsampwidth()*8, "bit")
# print("So kenh am thanh: ", sf.getnchannels())
# print("h", sf.getnframes)