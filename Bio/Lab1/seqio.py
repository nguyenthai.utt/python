import Bio.SeqIO as SeqIO

# seq_records = SeqIO.parse("ls_orchid.fasta", "fasta")
# # all_species = [record.description.split()[1] for record in seq_records]
# record = next(seq_records)
# print(record)
# my_seq = record.seq

# my_seq = my_seq[0:12]
# print(my_seq)
# mRNA = my_seq.transcribe()
# print(mRNA.translate())

# print(record.annotations.keys())

#5.4 Seq file as a dictionary
def getKey(orgirinalId):
    return orgirinalId.split("|")[3]

record_dict = SeqIO.index("ls_orchid.fasta", "fasta", key_function=getKey)

#5.5 Write Seq
my_records = list(SeqIO.parse("ls_orchid.fasta", "fasta"))
print(SeqIO.write(my_records, "my_record", "fasta"))