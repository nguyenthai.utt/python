import Bio
from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqUtils import GC

#Phiên bản hiện tại của thư viện Biopython đang sử  dụng
print(Bio.__version__)

#Tạo ra một đối tượng Sequence
# my_seq = Seq("AGTACACTGGT")
#In chuỗi Sequence 
# print(my_seq)
# print(my_seq.complement())
# print("Reverse complement: ", my_seq.reverse_complement())

# for seq_record in SeqIO.parse("ls_orchid.fasta", "fasta"):
#     print(seq_record.id)
#     print(repr(seq_record.seq))
#     print(seq_record.seq, " => ", len(seq_record.seq))
#     break

# for seq_record in SeqIO.parse("ls_orchid.gbk", "genbank"):
#     print(seq_record.id)
#     print(repr(seq_record.seq))
#     print(seq_record.seq, " => ", len(seq_record.seq))
#     break

# my_seq = Seq("GATCG")
# for index, letter in enumerate(my_seq):
#     print("%i %s" % (index, letter))
#     #print(index, letter)
#     #print(my_seq[index])

# print("Len sequence: ", len(my_seq))
# print(my_seq[0])
# print(my_seq[2])
# print(my_seq[-1])

# Non-overlapping count
# "AAAA".count("AA")

# my_seq = Seq("GATCGATGGGCCTATATAGGATCGAAAATCGC")
# print("Len my_seq: ", len(my_seq))
# print("Tổng số G: ", my_seq.count("G"))
# gc = 100 * float(my_seq.count("G") + my_seq.count("C")) / len(my_seq)
# print("Tỷ lệ GC: ", gc)

# my_seq = Seq("GATCGATGGGCCTATATAGGATCGAAAATCGC")
# print("Tỷ lệ GC: ", GC(my_seq))

# #Slicing a sequence
# my_seq = Seq("GATCGATGGGCCTATATAGGATCGAAAATCGC")
# my_seq[4:12]

# # get the first, second and third codon positions of this DNA sequence: 
# # với 3 ORF (Open Reading Frame)
# print(my_seq[0::3])
# print(my_seq[1::3])
# print(my_seq[2::3])

# print(my_seq[::-1])

# # Convert to FASTA format
# fasta_format_string = ">Name\n%s\n" % my_seq
# print("\nFASTA:\n", fasta_format_string)

# dna_seq_1 = Seq("ACGT")
# dna_seq_2 = Seq("CGTATG")
# dna_seq = dna_seq_1 + dna_seq_2
# print("dna_seq: ", dna_seq)

# list_of_seqs = [Seq("ACGT"), Seq("AACC"), Seq("GGTT")]
# concatenated = Seq("")
# for s in list_of_seqs:
#     concatenated += s
# print("concatenated: ", concatenated)

# join method 
# contigs = [Seq("ATG"), Seq("ATCCCG"), Seq("TTGCA")]
# spacer = Seq("N"*10)
# new_seq = spacer.join(contigs)
# print("new_seq: ", new_seq)

# # Changing case
# dna_seq = Seq("acgtACGT")
# dna_seq_upper = dna_seq.upper()
# dna_seq_lower = dna_seq.lower()
# print("\n dna_seq_upper: ", dna_seq_upper)
# print(" dna_seq_lower: ", dna_seq_lower)

# # Nucleotide sequences and (reverse) complements
# my_seq = Seq("ACGGTA")
# print("\n Complement: ", my_seq.complement())
# print("Reverse complement: ", my_seq.reverse_complement())

# coding_dna = Seq("ATGGCC")
# print("coding_dna:    ", coding_dna)
# template_dna = coding_dna.reverse_complement()
# print("template_dna:  ", template_dna)

# messenger_rna = coding_dna.transcribe()
# print("messenger_rna: ", messenger_rna)

# # The Seq object also includes a back-transcription method for going from the mRNA to the coding strand of the DNA. Again, this is a simple U → T substitution:
# print("back_transcribe: ", messenger_rna.back_transcribe())

# # Translation Tables
# from Bio.Data import CodonTable
# standard_table = CodonTable.unambiguous_dna_by_name["Standard"]
# #standard_table = CodonTable.unambiguous_dna_by_id[1]

# mito_table = CodonTable.unambiguous_dna_by_name["Vertebrate Mitochondrial"]
# mito_table = CodonTable.unambiguous_dna_by_id[2]

# print(standard_table)
# #print(mito_table)

# print("Stop codons: ", standard_table.stop_codons)
# print("Start codons: ", standard_table.start_codons)


# messenger_rna = Seq("AUGGCCAUUGUAAUGGGCCGCUGAAAGGGUGCCCGAUAG")
# protein_seq = messenger_rna.translate()
# print("protein_seq: ", protein_seq)
# print(messenger_rna.translate(to_stop=True))

# You can also translate directly from the coding strand DNA sequence:
# coding_dna = Seq("ATGGCCATTGTAATGGGCCGCTGAAAGGGTGCCCGATAG")
# protein_seq = coding_dna.translate(to_stop=True)
# print("protein_seq: ", protein_seq)

# # Comparing Seq objects
# seq1 = Seq("ACGT")
# print("ACGT" == seq1)
# print(seq1 == "ACGT")

import matplotlib.pyplot as plt
import numpy as np
def factorial(n):
    return np.prod(range(1, n+1))
k = 10
mean = np.arange(20)
pk = np.power(mean, k)*np.exp(-mean)/factorial(k)

plt.plot(pk, k)
plt.show()